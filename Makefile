build:
	go mod download
	go build -o bin/bstatus src/cmd/bstatus/main.go
	go build -o bin/bstatuscli src/cmd/bstatuscli/client.go

configure:
	bash src/scripts/configFolder.sh
	cp src/config/config.toml $(HOME)/.config/bstatus

install:
	cp bin/* $(GOPATH)/bin

dev:
	go run src/cmd/bstatus/main.go
