# BSTATUS (Battery Status)

This is a little golang application that origins from the need to get information such as:

- Battery %
- Date and Time
- SSID of the Wifi network

This idea came along when I stoped using bars in my desktop, so I dont have an easy way to check this information

You can call it using **bstatus show** so it send a notification with all that information, or you cant just execute as a daemon in second plane
and it will send the notification every time the bnattery level drops 5%

## Installation

```bash
go get -v https://gitlab.com/wenceslao1207/bstatus
```


### From Source:
```bash
git clone https://gitlab.com/wenceslao1207/bstatus.git
cd bstatus
make build
make configure
make install
``` 
