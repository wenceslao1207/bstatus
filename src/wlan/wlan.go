package wlan

import (
	"os/exec"
	"strings"
)

type Wlan struct {
	ssid    string
	message string
}

func (w *Wlan) SetSSID() {
	ssid, err := exec.Command("/usr/sbin/iwgetid", "-r").Output()
	if err != nil {
		w.ssid = "Disconnected"
	} else {
		w.ssid = string(ssid)
	}
}

func (w *Wlan) GetMessage() string {
	w.message = strings.Join([]string{"Wifi: ", w.ssid}, "")
	return w.message
}
