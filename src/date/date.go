package date

import (
	"strconv"
	"strings"
	"time"
)

type CurrentDate struct {
	day     string
	month   string
	hour    string
	minutes string
	message string
}

func NewCurrentDate() *CurrentDate {
	cd := CurrentDate{}
	cd.SetDate()
	return &cd
}

func (cd *CurrentDate) SetDate() {
	curr_time := time.Now()
	day := curr_time.Day()
	month := curr_time.Month().String()
	hour := curr_time.Hour()
	minutes := curr_time.Minute()

	cd.day = strconv.Itoa(day)
	cd.month = month
	cd.hour = strconv.Itoa(hour)
	cd.minutes = strconv.Itoa(minutes)
}

func (cd *CurrentDate) GetMessage() string {
	cd.message = strings.Join([]string{"Date: ", cd.month, " ", cd.day, " ", cd.hour, ":", cd.minutes}, "")
	return cd.message
}
