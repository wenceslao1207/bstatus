package notification

import (
	"os/exec"
)

type Notification struct {
	message string
	icon    string
	title   string
}

func NewNotification(icon string, title string) *Notification {
	return &Notification{
		icon:  icon,
		title: icon,
	}
}

func (n *Notification) SetMessage(message string) {
	n.message = message
}

func (n *Notification) SetIcon(dir string) {
	n.icon = dir
}

func (n *Notification) SetTitle(title string) {
	n.title = title
}

func (n *Notification) Notify() error {
	_, err := exec.Command("/usr/bin/notify-send", "-i", n.icon, n.title, n.message).Output()
	return err
}
