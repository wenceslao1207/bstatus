package keyboard

import (
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"regexp"
	"time"

	. "gitlab.com/wenceslao1207/bstatus/src/notification"
)

func main() {

	log.Println("test")
}

type keyboard struct {
	fileNames string
	quantity  int
	message   string
}

func NewKeyboard(fileNames string) *keyboard {
	return &keyboard{fileNames: fileNames}
}

func (k *keyboard) GetFileNames() string {
	return k.fileNames
}

func (k *keyboard) SetFileNames(fileNames string) {
	k.fileNames = fileNames
}

func (k *keyboard) SetQuantity(quantity int) {
	k.quantity = quantity
}

func (k *keyboard) GetQuantity() int {
	return k.quantity
}

func (k *keyboard) Configure() {
	_, err := exec.Command("/bin/bash", "-c", "xmodmap ~/.xmodmaprc").Output()
	if err != nil {
		log.Printf("%s\n", err)
	}
}

func (k *keyboard) SetMessage(message string) {
	k.message = message
}

func (k *keyboard) GetMessage() string {
	return k.message
}

func (k *keyboard) Watcher(c chan struct{}) {
	for {
		quantity, err := k.FindFiles()
		if err != nil {
			log.Print(err.Error())
		}
		switch {
		case (quantity > k.GetQuantity()):
			k.Configure()
			k.SetMessage("Keyboard connected, configured and ready for use.")
			c <- struct{}{}
		case quantity < k.GetQuantity():
			k.SetMessage("Keyboard disconnected")
			c <- struct{}{}
		}
		k.SetQuantity(quantity)
		time.Sleep(2 * time.Second)
	}
}

func (k *keyboard) Notify(notification Notification) {
	message := k.GetMessage()
	notification.SetMessage(message)
	notification.Notify()
}

func (k *keyboard) FindFiles() (int, error) {
	quantity := 0
	re := regexp.MustCompile(k.fileNames)

	files, err := ioutil.ReadDir("/dev")
	if err != nil {
		log.Println(err)
	}

	for _, f := range files {
		fileName := f.Name()
		if flag := re.Match([]byte(fileName)); flag {
			quantity++
		}
	}

	if quantity == 0 {
		return 0, fmt.Errorf("There was no file that match this expression: %s\n", k.fileNames)
	}

	return quantity, nil
}
