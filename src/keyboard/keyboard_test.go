package keyboard

import "testing"

func TestDaemon(t *testing.T) {
	var quantity int
	var err error

	kb := NewKeyboard("hidraw")

	quantity, err = kb.FindFiles()

	if err != nil || quantity == 0 {
		t.Errorf("%s\n", err)
	}

	kb.Configure()

	kb.Watcher()
	kb.SetFileNames("filename")

	quantity, err = kb.FindFiles()

	if err == nil || quantity > 0 {
		t.Errorf("%s\n", err)
	}

	if quantity != kb.GetQuantity() {
		t.Errorf("%s\n", err)
	}

}
