#/bin/sh

FOLDER=$HOME/.config/bstatus

if [ ! -d "$FOLDER" ]; then
	echo "CREATING CONFIG FOLDER $FOLDER"
	mkdir "$FOLDER"
fi
