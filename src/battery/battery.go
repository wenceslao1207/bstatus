package battery

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	. "gitlab.com/wenceslao1207/bstatus/src/config"
)

type Battery struct {
	capacityFile string
	statusFile   string
	percentage   int
	message      string
	critical     int
	status       string
	delta        string
}

// Constructor
func NewBattery(capacity string, status string, delta string) *Battery {
	return &Battery{
		capacityFile: capacity,
		statusFile:   status,
		delta:        delta,
	}
}

func (b *Battery) GetStatus() string {
	return b.status
}

func (b *Battery) GetMessage() string {
	return b.message
}

func (b *Battery) GetPercentage() int {
	return b.percentage
}

func (b *Battery) GetCritical() int {
	return b.critical
}

func (b *Battery) GetState() bool {
	if b.status == "Charging" {
		b.SetCritical()
		return false
	}
	if b.percentage > b.critical {
		return false
	}
	if b.critical <= 25 {
		b.SetMessage("LOW BATTERY --> Please connect the charger \n")
	} else {
		b.SetMessage("")
	}
	b.SetCritical()
	return true
}

func (b *Battery) Setup() error {
	err := b.SetCapacity()
	if err != nil {
		return err
	}

	b.SetStatus()
	return nil
}

func (b *Battery) SetMessage(message string) string {
	var messageOut bytes.Buffer
	battery := strconv.Itoa(b.percentage)
	messageOut.Reset()
	messageOut.WriteString(b.delta)
	messageOut.WriteString(" Battery at: ")
	messageOut.WriteString(battery)
	messageOut.WriteString("% ")
	messageOut.WriteString("\n")
	messageOut.WriteString("Status: ")
	messageOut.WriteString(b.status)
	messageOut.WriteString("\n\n")
	messageOut.WriteString(message)
	b.message = messageOut.String()
	return b.message
}

func (b *Battery) SetCapacity() error {
	data, err := ioutil.ReadFile(b.capacityFile)
	if err != nil {
		return fmt.Errorf("Error no file founded")
	}
	charge := strings.Split(string(data), "\n")
	battery, err := strconv.Atoi(charge[0])
	if err != nil {
		panic(err)
	}
	if battery > 100 {
		battery = 100
	}
	b.percentage = battery
	return nil
}

func (b *Battery) SetStatus() {
	data, err := ioutil.ReadFile(b.statusFile)
	if err != nil {
		panic(err)
	}
	output := strings.Split(string(data), "\n")
	if output[0] == "Not charging" {
		b.status = "Fully Charged"
	} else {
		if output[0] == "Unknown" {
			b.status = "Discharged"
		} else {
			b.status = output[0]
		}
	}
}

func (b *Battery) SetCritical() {
	critical := ((b.percentage - 1) / 5)
	critical = critical * 5
	b.critical = critical
}

// maybe its better if it outpurts a boolean to know when to show a notification.
func (b *Battery) Watcher(c chan struct{}) {
	for {
		if b.GetState() {
			c <- struct{}{}
		}
		time.Sleep(2 * time.Second)
		prev := b.GetStatus()
		b.SetStatus()

		if prev != b.GetStatus() {
			c <- struct{}{}
		}

		prevBattery := b.GetPercentage()
		currentBattery := b.GetPercentage()
		if prevBattery != currentBattery && currentBattery == 100 {
			c <- struct{}{}
		}
		b.SetCapacity()
	}
	close(c)
}

func BatteryGenerator(config map[string]BatteryConfig) ([]Battery, error) {
	batteries := []Battery{}

	if len(config) == 0 {
		return batteries, fmt.Errorf("%s\n", "No batteries in this system")
	}

	for _, value := range config {
		capacity := fmt.Sprintf("/sys/class/power_supply/%s/%s", value.SystemName, value.CapacityFile)
		status := fmt.Sprintf("/sys/class/power_supply/%s/%s", value.SystemName, value.StatusFile)

		battery := NewBattery(
			capacity,
			status,
			value.Delta,
		)

		err := battery.SetCapacity()
		if err != nil {
			return []Battery{}, err
		}

		battery.SetStatus()
		battery.SetCritical()
		batteries = append(batteries, *battery)
	}

	return batteries, nil

}
