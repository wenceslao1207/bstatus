package battery

import (
	"bytes"
	"io/ioutil"
	"strconv"
	"strings"
)

type Battery struct {
	capacity_file string
	status_file   string
	percentage    int
	message       string
	critical      int
	status        string
}

// Constructor
func New_Battery() *Battery {
	return &Battery{
		"/sys/class/power_supply/BAT1/capacity",
		"/sys/class/power_supply/BAT1/status",
		0,
		"",
		0,
		"",
	}
}

func New_SecondBattery() *Battery {
	return &Battery{
		"/sys/class/power_supply/BAT2/capacity",
		"/sys/class/power_supply/BAT2/status",
		0,
		"",
		0,
		"",
	}
}

func (b *Battery) GetStatus() string {
	return b.status
}

func (b *Battery) GetMessage() string {
	return b.message
}

func (b *Battery) GetPecentage() int {
	return b.percentage
}

func (b *Battery) GetCritical() int {
	return b.critical
}

func (b *Battery) GetSate() bool {
	if b.status == "Charging" {
		b.SetCritical()
		return false
	}
	if b.percentage > b.critical {
		return false
	}
	if b.critical <= 25 {
		b.SetMessage("LOW BATTERY --> Please connect the charger \n")
	} else {
		b.SetMessage("")
	}
	b.SetCritical()
	return true
}

func (b *Battery) SetMessage(message string) string {
	var messageOut bytes.Buffer
	battery := strconv.Itoa(b.percentage)
	messageOut.Reset()
	messageOut.WriteString("Battery at: ")
	messageOut.WriteString(battery)
	messageOut.WriteString("% ")
	messageOut.WriteString("\n")
	messageOut.WriteString("Status: ")
	messageOut.WriteString(b.status)
	messageOut.WriteString("\n")
	messageOut.WriteString(message)
	b.message = messageOut.String()
	return b.message
}

func (b *Battery) SetCapacity() {
	data, err := ioutil.ReadFile(b.capacity_file)
	if err != nil {
		panic(err)
	}
	charge := strings.Split(string(data), "\n")
	battery, err := strconv.Atoi(charge[0])
	if err != nil {
		panic(err)
	}
	if battery > 100 {
		battery = 100
	}
	b.percentage = battery
}

func (b *Battery) SetStatus() {
	data, err := ioutil.ReadFile(b.status_file)
	if err != nil {
		panic(err)
	}
	output := strings.Split(string(data), "\n")
	if output[0] == "Not charging" {
		b.status = "Fully Charged"
	} else {
		b.status = output[0]
	}
}

func (b *Battery) SetCritical() {
	critical := ((b.percentage - 1) / 5)
	critical = critical * 5
	b.critical = critical
}
