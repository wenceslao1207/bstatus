package server

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/wenceslao1207/bstatus/src/battery"
	"gitlab.com/wenceslao1207/bstatus/src/config"
	"gitlab.com/wenceslao1207/bstatus/src/date"
	"gitlab.com/wenceslao1207/bstatus/src/keyboard"
	"gitlab.com/wenceslao1207/bstatus/src/notification"
	"gitlab.com/wenceslao1207/bstatus/src/wlan"
)

func Server(config config.Config) {
	// Set current Date.
	date := date.NewCurrentDate()
	// set current Wifi. this doesn't work but ok
	wlan := wlan.Wlan{}
	// Create Notification Manager
	notification := notification.Notification{}
	notification.SetTitle("G-Status ")

	icon := os.Getenv("GOPATH") + "/src/gitlab.com/wenceslao1207/bstatus/icon.png"
	notification.SetIcon(icon)

	kb := keyboard.NewKeyboard("hidraw")
	kChannel := make(chan struct{})
	bChannel := make(chan struct{})

	batteries, err := battery.BatteryGenerator(config.Batteries)
	if err != nil {
		log.Println(err.Error())
	} else {
		for _, value := range batteries {
			// set status of batteries.
			err = value.Setup()
			if err != nil {
				log.Println(err.Error())
			}
			go value.Watcher(bChannel)
		}
	}
	go kb.Watcher(kChannel)

	connection := SetConnection("/tmp/bstatus.sock")

	for {
		select {
		case <-bChannel:
			go CallNotification(*date, wlan, batteries, notification, false)
		case <-kChannel:
			kb.Notify(notification)
		}
		go ClientDaemon(connection, bChannel)
	}
}

func SetConnection(socket string) net.Listener {
	ClearSocket(socket)
	connection, err := net.Listen("unix", socket)
	if err != nil {
		log.Println(err.Error())
	}
	return connection
}

func SocketServer(conn net.Conn, bChannel chan struct{}) {
	for {
		buffer := make([]byte, 512)
		nr, err := conn.Read(buffer)
		if err != nil {
			log.Println(err.Error())
			return
		}

		data := buffer[0:nr]
		if string(data) == "GO" {
			bChannel <- struct{}{}
		}
	}
}

func ClearSocket(socket string) {
	if _, err := os.Stat(socket); err == nil {
		err := os.Remove(socket)
		if err != nil {
			log.Println(err.Error())
		}

	}

}

func ClientDaemon(connection net.Listener, bChannel chan struct{}) {
	fd, err := connection.Accept()
	if err != nil {
		log.Println(err.Error())
	}
	go SocketServer(fd, bChannel)
}

func createMessageNotification(
	batteryMessage []string,
	wlan string,
	date string,
) string {
	var message string
	for _, value := range batteryMessage {
		message = fmt.Sprintf("%s%s", message, value)
	}
	message = fmt.Sprintf("%s%s \n", message, wlan)
	message = fmt.Sprintf("%s%s \n", message, date)
	return message
}

func CallNotification(
	date date.CurrentDate,
	wlan wlan.Wlan,
	batteries []battery.Battery,
	notification notification.Notification,
	flag bool,
) {
	batteryMessage := []string{}
	date.SetDate()
	wlan.SetSSID()
	if flag {
		for _, value := range batteries {
			value.SetMessage("")
		}
	}
	for _, value := range batteries {
		value.Setup()
		value.GetState()
		value.SetMessage("")
		batteryMessage = append(batteryMessage, value.GetMessage())
	}
	message := createMessageNotification(
		batteryMessage,
		wlan.GetMessage(),
		date.GetMessage(),
	)

	notification.SetMessage(message)
	err := notification.Notify()
	if err != nil {
		fmt.Println(err.Error())
	}
}
