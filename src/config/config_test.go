package config

import (
	"log"
	"os/user"
	"reflect"
	"testing"
)

func TestConfig(t *testing.T) {

	usr, err := user.Current()

	if err != nil {
		t.Errorf("%s\n", err.Error())
	}

	home := usr.HomeDir
	log.Println(reflect.TypeOf(home))

	config, err := ReadConfigFile("config.toml")
	if err != nil {
		t.Errorf("%s\n", err.Error())
	}
	log.Println(config)

}
