package config

import (
	"bytes"
	"log"
	"os/user"

	"github.com/BurntSushi/toml"
)

type BatteryConfig struct {
	Name         string
	SystemName   string
	CapacityFile string
	StatusFile   string
	Delta        string
}

type Config struct {
	Batteries map[string]BatteryConfig
}

func GetConfiguration(configFile string) Config {
	var err error
	var buffer bytes.Buffer

	usr, err := user.Current()
	if err != nil {
		log.Println(err.Error())
	}

	homeDir := usr.HomeDir
	buffer.WriteString(homeDir)
	buffer.WriteString(configFile)

	result, err := ReadConfigFile(buffer.String())
	if err != nil {
		log.Println(err.Error())
	}
	return *result
}

func ReadConfigFile(configFile string) (*Config, error) {
	var config Config
	if _, err := toml.DecodeFile(configFile, &config); err != nil {
		return nil, err
	}
	return &config, nil

}
