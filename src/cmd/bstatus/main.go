package main

import (
	"gitlab.com/wenceslao1207/bstatus/src/config"
	"gitlab.com/wenceslao1207/bstatus/src/server"
)

const configFile = "/.config/bstatus/config.toml"

var configuration config.Config

func init() {
	configuration = config.GetConfiguration(configFile)
}

func main() {
	server.Server(configuration)
}
