package main

import (
	"fmt"
	"net"
)

func recoverFromPanic() {
	if rec := recover(); rec != nil {
		fmt.Println("No argument was passed")
	}
}

func main() {

	conn, err := net.Dial("unix", "/tmp/bstatus.sock")
	if err != nil {
		panic(err)
	}

	defer conn.Close()
	defer recoverFromPanic()
	conn.Write([]byte("GO"))
}
